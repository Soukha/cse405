// Phimpasouk, Soukha
// CSE405: Server Programming
// Psql: Assignment 5 
// sam.sql
// January 27, 2018
// Modified Code of Dr. David Turner. 

create table Beatles (
	id int primary key,
	Album char(30),
	Year int);

insert into Beatles values (1, 'The White Album', 1968);
insert into Beatles values (2, 'Let it Be', 1970);
insert into Beatles values (3, 'Rubber Soul', 1965);

select * from Beatles;

select * from Beatles where id= 2;

update Beatles set Album = 'Revolver' where id= 3;
update Beatles set Year = 1966 where id = 3;

select * from Beatles;

delete from Beatles where id= 2;

select * from Beatles;