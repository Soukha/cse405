// Phimpasouk, Soukha
// CSE405: Server Programming
// HTTP: Assignment 3: server2.js
// January 19, 2018
// Modified code of Dr. David Turner

const http = require('http');
const net = require('net');
const url = require('url');

const html =
  "<html>"                           +
  "<head><title>405</title></head>"  +
  "<body><h1>Hi</h1></body>"         +
  "</html>"; 

const server = http.createServer((req, res) => {
  res.setHeader('Content-Type', 'text/html');
  res.end(html);
});

server.listen(8000, () => {
  console.log('Server: Open the following URL in browser:');
  console.log('Server: http://localhost:8000/');
  console.log('');
});                 

const serverResponse = 
  'HTTP/1.1 200 OK'          + '\r\n' +  // status line
  'Content-length: 11'       + '\r\n' +  // general header
  'Content-type: text/html'  + '\r\n' +  // general header
  '\r\n';  // blank line

server.on('connection', (serverSocket) => {
  console.log('Server: Something connected to me.');
  serverSocket.on('data', (data) => {
    console.log('Server: Received from client:\n' + data);
    serverSocket.end(serverResponse); // close connection.
  });
});