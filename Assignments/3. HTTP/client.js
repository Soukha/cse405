// Phimpasouk, Soukha
// CSE405: Server Programming
// HTTP: Assignment 3: client.js
// January 19, 2018

const net = require('net');
const clientSocket = net.createConnection({ port: 8000 }, () => {
  //'connect' listener
  console.log('Client: Connected to server!');
  clientSocket.write('Client: Hello World!\r\n');
});

clientSocket.on('data', (chunk) => {
  console.log('Client: data from Server:');
  console.log(chunk.toString('utf8'));
  clientSocket.end();
});

clientSocket.on('end', () => {
	console.log('Client: Disconnected from server');
  });

clientSocket.on('connect', () => {
 	console.log('Client: const req =');
 	const req =
 	  'GET / HTTP/1.1  \r\n' +
 	  'Host: localhost \r\n' +
	  '\r\n';
 	clientSocket.end(req);  // Send req string and then close this side of the TCP socket.
});

