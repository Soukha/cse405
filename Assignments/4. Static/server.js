// Phimpasouk, Soukha
// CSE405: Server Programming
// Static: Assignment 4: 
// server.js
// January 23, 2018
// Modified code of Dr. David Turner

const http = require('http');
const st = require('st');

const port = 8000;

var mount = st({
  path: 'public/',
	index: '/index.html',
	hi: '/hi.html'
});

const server = http.createServer(function(req, res) {
  mount(req, res);
	if(req.url === "/index"){
		sendFileContent(res, "index.html", "text/html");
	}
	else if(req.url === "public/"){
		sendFileContent(res, "index.html", "text/html");
	}
	else if(req.url === "hi"){
		sendFileContent(res, "hi.html", "text/html");
	}
});

server.listen(port, () => {
				console.log('')
				console.log('3 url choices:')
				console.log('Server running at...');
				console.log(`http://localhost:${port}/`);
				console.log(`http://localhost:${port}/index.html`);
				console.log(`http://localhost:${port}/hi.html`);
});
