const pg = require('pg');

process.env.PGDATABASE = 'db';
process.env.PGUSER = 'postgres';
process.env.PGPASSWORD = 'jjj';  // pg database password

const pool = new pg.Pool();

pool.on('error', (err, client) => {
    console.log(err.stack);
    process.exit(-1);
});

pool.connect((err, client, done) => {
  if (err) throw err;
  const q = ' Create table users (                ' +
            ' username varchar(255) primary key,  ' +
            ' password varchar(255) not null,     ' +
            ' color char(6)                       ' +
            ')                                    ';
  client.query(q, (err) => {
    if (err) throw err;
    insertUser(client, done);
  });
});

function insertUser(client, done) {
  const q = "insert into users values ('fred', '1234', '0000FF')";
  client.query(q, (err) => {
    if (err) throw err;
    done();
    pool.end();
  });
}

function myEnd(client, done) {
  done();
  pool.end();
}