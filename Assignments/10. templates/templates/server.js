const http      = require('http');
const qs        = require('querystring');
const st        = require('st');
const fs        = require('fs');
const sessions  = require('./sessions');

const mount = st({
  path: 'public/'
});

const homeTemplate  = fs.readFileSync('views/home.html', 'utf8');
const colorTemplate = fs.readFileSync('views/color.html', 'utf8');

const server = http.createServer();

server.listen(8000);

server.on('request', (req, res) => {
  sessions.filter(req, res);
  if(req.url === '/login.html') {
    console.log("/login.html");
    mount(req, res);
  } else if (req.url === '/login') {
    console.log("/login");
    handleLoginFormSubmission(req, res);
  }else if (!req.session.hasOwnProperty('username')) {
    console.log("username");
    redirectLoginPage(res);
  }else if (req.url === '/logout') {
    console.log("/logout");
    handleLogoutFormSubmission(req, res);
  }else if(req.url === '/color') {
    console.log("/color");
    handleColorFormSubmission(req, res);
  }else if(req.url === '/color.html') {
    console.log("/color.html");
    handleColorPage(req, res);
  }else if (req.url === '/') {
    console.log("req.url === '/'");
    handleHomePage(req, res);
  } else {
    console.log("else");
    mount(req, res);
  }
});

function handleHomePage(req, res) {
  res.setHeader('Content-type', 'text/html');
  const html = homeTemplate.replace('#color#', req.session.color);
  res.end(html);
}

function handleColorPage(req, res) {
  var html;
  switch (req.session.color) {
    case 'FF0000':
      console.log("Red Checked");
      html = colorTemplate.replace('#red#'   , 'checked');
      html = html         .replace('#green#' , '       ');
      html = html         .replace('#blue#'  , '       ');
      break;
    case '00FF00':
      console.log("Green Checked");
      html = colorTemplate.replace('#red#'   , '       ');
      html = html         .replace('#green#' , 'checked');
      html = html         .replace('#blue#'  , '       ');
      break;
    case '0000FF':
      console.log("Blue Checked");
      html = colorTemplate.replace('#red#'   , '       ');
      html = html         .replace('#green#' , '       ');
      html = html         .replace('#blue#'  , 'checked');
      break;
    default:
      console.log("None Checked");
      html = colorTemplate.replace('#red#'   , '       ');
      html = html         .replace('#green#' , '       ');
      html = html         .replace('#blue#'  , '       ');
      break;
  }
  res.setHeader('Content-type', 'text/html');
  res.end(html);
}
function handleLoginFormSubmission(req, res) {
  let body = '';
  req.on('data', (chunk) => {
    body += chunk;
  });
  req.on('end', () => {
    const form = qs.parse(body);
    if (form.username !== 'fred' || form.password !== '1234') {
      redirectLoginPage(res);
    } else {
      // User submitted correct username and password.
      req.session.username = form.username;
      redirectHomePage(res);
    }
  });
}

function handleLogoutFormSubmission(req, res) {
  delete req.session.username;
  redirectLoginPage(res);
}

function redirectLoginPage(res) {
  res.statusCode = 301;
  res.setHeader('Location', '/login.html');
  res.end();
}
function redirectHomePage(res) {
  res.statusCode = 301;
  res.setHeader('Location', '/');
  res.end();
}

function handleColorFormSubmission(req, res) {
  let body = '';
  req.on('data', (chunk) => {
    body += chunk;
  });
  req.on('end', () => {
    const form = qs.parse(body);
    if (
      form.color !== 'FF0000' &&
      form.color !== '00FF00' &&   
      form.color !== '0000FF'
    ) {
      res.writeHead(400);
      res.end('Bad request');
    }
    req.session.color = form.color;
    res.writeHead(302, { 'Location': '/' });
    res.end();
  });
}