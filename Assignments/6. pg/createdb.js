// Phimpasouk, Soukha
// CSE405: Server Programming
// pg: Assignment 6: 
// createdb.js
// Febuary 2, 2018

const pg = require('pg');
process.env.PGDATABASE = 'pg';
process.env.PGUSER = 'postgres';
process.env.PGPASSWORD = '333';  // pg database password

const pool = new pg.Pool();

pool.on('error', (err, client) => {
    console.log(err.stack);
      process.exit(-1);
});

// utility function
function insertRow(client, id, x, cb) {
    client.query("insert into test values ($1::text, $2)", [id, x], (err) => {
            if (err) throw err; else cb();
    });
}

function selectAll(client, cb) {
  client.query("select * from test order by id",(err, result) => {
            if (err) throw err;
              result.rows.map((row, i) => {
                  console.log('  ' + i + " " + row.id + ' ' + row.x);
              });
            cb();
  });
}

function selectById(client, id, x, cb) {
  client.query("select * from test where id=$1::text", [id, x], (err) => {
    if (err) throw err; 
      console.log("\nselect * from test where id = ?");
    res.rows.forEach((row) => {
                 console.log(row,id + ' ' + row.x);
    });
  });
}

function updateRow(client, id, x, cb) {// set x for given id
  client.query("update test set x = $1 where d = $2::text", [x, id], (err) => {
        if (errr) throw err;
        cb();
    });
}

function deleteRow(client, id, cb) {  // delete row for given id
  client.query("delete from test where id = $1::text", [id], (err) => {
    if (err) throw err;
    cb();
  });
}

//main function

// Get a database connection for the pool.
pool.connect((err, client, done) => {
    if (err) throw err;
    createTest(client, done);
});

// Create table test.
function createTest(client, done) {
  const q = 'create table test (          ' +
            'id varchar(255) primary key, ' +
            'x integer                    ' +
            ')                            ';
  client.query(q, (err) => {
    if (err) throw err; else insertA(client, done);
  });
}

function insertA(client, done) {
  insertRow(client, 'a', 1, () => {
    insertB(client, done);
  });
}

function insertB(client, done) {
  insertRow(client, 'b', 2, () => {
    insertC(client, done);
    });
}

function insertC(client, done) {
  insertRow(client, 'c', 3, () => {
    done();
    pool.end();
  });
}

function selectB(client, done) {
  selectById(client, 'b', () => {
    updateB(client, done);
  });
}

function updateB(client, done) {
  updateRow(client, 'b', 22, () => {
    done();
    mySelectAll(client, done);
  });
}

function mySelectAll(client, done) {
  selectAll(client,() => {
    deleteB(client, done);
  });
}

function deleteB(client, done) {
  deleteRow(client, 'b',() => {
    selectAll2(client,done);
  });
}

function selectAll2(client, done) {
  selectAll(client, () => {
    myEnd(client, done);
  });
}

function myEnd(client, done) {
  done();
  pool.end();
}
